<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 14.05.2016
 * Time: 10:37
 */

namespace app\controllers;

use Yii;
use yii\App\Controller;
use app\models\ContactForm;
use yii\web\Request;



class IndexController extends AppController
{
// экшен корзины
    public function actionContact()
    {
        
        //$this->layout = 'contact';
        $this->layout = false;
        

          /* Создаем экземпляр класса */
        $model = new ContactForm();
        /* получаем данные из формы и запускаем функцию отправки contact, если все хорошо, выводим сообщение об удачной отправке сообщения на почту */
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        /* иначе выводим форму обратной связи */
        } else {
           return $this->render('contact', [
               'model' => $model,
         ]);
           /*  return $this->render('contact', compact('contact'));*/
        }
    }
}

   /*  public function actionView(){
       
       
        // выводим title E-SHOPPER и прицепляем название категории
        /* $this->setMeta('E-SHOPPER | ' . $category->name, $category->keywords, $category->description);

        // рендерим category/view и передаем в него найденные продукты
        return $this->render('index', compact('contact'));
    }
}
*/
     
   
       

       