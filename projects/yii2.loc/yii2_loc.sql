-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 12 2018 г., 17:26
-- Версия сервера: 5.5.53-log
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yii2_loc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `keywords`, `description`) VALUES
(1, 0, 'Sportswear', NULL, NULL),
(2, 0, 'Mens', NULL, NULL),
(3, 0, 'Womens', NULL, NULL),
(4, 1, 'Nike', NULL, NULL),
(5, 1, 'Under Armour', NULL, NULL),
(6, 1, 'Adidas', NULL, NULL),
(7, 1, 'Puma', NULL, NULL),
(8, 1, 'ASICS', NULL, NULL),
(9, 2, 'Fendi', NULL, NULL),
(10, 2, 'Guess', NULL, NULL),
(11, 2, 'Valentino', NULL, NULL),
(12, 2, 'Dior', NULL, NULL),
(13, 2, 'Versace', NULL, NULL),
(14, 2, 'Armani', NULL, NULL),
(15, 2, 'Prada', NULL, NULL),
(16, 2, 'Dolce and Gabbana', NULL, NULL),
(17, 2, 'Chanel', NULL, NULL),
(18, 2, 'Gucci', NULL, NULL),
(19, 3, 'Fendi', NULL, NULL),
(20, 3, 'Guess', NULL, NULL),
(21, 3, 'Valentino', NULL, NULL),
(22, 3, 'Dior', NULL, NULL),
(23, 3, 'Versace', NULL, NULL),
(24, 0, 'Kids', NULL, NULL),
(25, 0, 'Fashion', NULL, NULL),
(26, 0, 'Households', NULL, NULL),
(27, 0, 'Interiors', NULL, NULL),
(28, 0, 'Clothing', NULL, NULL),
(29, 0, 'Bags', 'сумки ключевики...', 'сумки описание...'),
(30, 0, 'Shoes', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `contactform`
--

CREATE TABLE `contactform` (
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `subject` tinytext NOT NULL,
  `body` text NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `filePath` varchar(400) NOT NULL,
  `itemId` int(11) DEFAULT NULL,
  `isMain` tinyint(1) DEFAULT NULL,
  `modelName` varchar(150) NOT NULL,
  `urlAlias` varchar(400) NOT NULL,
  `name` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `image`
--

INSERT INTO `image` (`id`, `filePath`, `itemId`, `isMain`, `modelName`, `urlAlias`, `name`) VALUES
(1, 'Products/Product1/3b2ddc.jpg', 1, 0, 'Product', '74f4fc2ba3-3', ''),
(2, 'Products/Product1/22f5af.jpg', 1, 0, 'Product', 'c3296b0f07-4', ''),
(3, 'Products/Product2/c3de96.jpg', 2, 0, 'Product', '1825a3b600-3', ''),
(4, 'Products/Product2/298e1e.jpg', 2, 0, 'Product', 'de96584dd3-4', ''),
(5, 'Products/Product2/d3d857.jpg', 2, 0, 'Product', 'ebad782d6f-5', ''),
(6, 'Products/Product2/d0c265.jpg', 2, 0, 'Product', 'e4ad8872b7-6', ''),
(7, 'Products/Product2/9d092a.jpg', 2, 0, 'Product', 'fdeea9c495-7', ''),
(8, 'Products/Product2/89135b.jpg', 2, 0, 'Product', '9df051288b-8', ''),
(9, 'Products/Product2/953a7e.jpg', 2, 0, 'Product', '89d1229c7a-9', ''),
(10, 'Products/Product1/1a3be4.jpg', 1, 0, 'Product', '5dfb0ea228-5', ''),
(11, 'Products/Product1/18a104.jpg', 1, 0, 'Product', '753a302332-6', ''),
(12, 'Products/Product1/ab714f.jpg', 1, 0, 'Product', 'd0b7a9d746-2', ''),
(13, 'Products/Product1/73cc71.jpg', 1, 1, 'Product', '74cc234c49-1', ''),
(14, 'Products/Product5/1e1ad9.jpg', 5, 0, 'Product', '16f3cfcd24-3', ''),
(15, 'Products/Product5/5c2d60.jpg', 5, 0, 'Product', 'b16678c5cd-2', ''),
(16, 'Products/Product5/4b22a6.jpg', 5, 1, 'Product', '925a6b9ee4-1', ''),
(17, 'Products/Product3/d1a7f7.jpg', 3, 0, 'Product', '4253f8e928-2', ''),
(18, 'Products/Product3/90769c.jpg', 3, 0, 'Product', '2601926d84-3', ''),
(19, 'Products/Product3/48fcff.jpg', 3, 0, 'Product', 'c15a99175b-4', ''),
(20, 'Products/Product3/acbd47.jpg', 3, 1, 'Product', '8adf3db04d-1', ''),
(21, 'Products/Product7/ebdfaa.jpg', 7, 1, 'Product', '4c1103fa1f-1', ''),
(22, 'Products/Product7/490fbf.jpg', 7, NULL, 'Product', '653fcb46af-2', ''),
(23, 'Products/Product7/b3b01a.jpg', 7, NULL, 'Product', '263d0eaea3-3', ''),
(24, 'Products/Product2/069a57.jpg', 2, 0, 'Product', '24becdee72-10', ''),
(25, 'Products/Product2/ee7c53.jpg', 2, 0, 'Product', 'a9242282d0-2', ''),
(26, 'Products/Product6/f979f6.jpg', 6, 1, 'Product', 'd7055c5a3d-1', ''),
(27, 'Products/Product2/1e493e.jpg', 2, 1, 'Product', '3b6c1b3f58-1', '');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1537902971),
('m140622_111540_create_image_table', 1537902990),
('m140622_111545_add_name_to_image_table', 1537902990);

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `qty` int(10) NOT NULL,
  `sum` float NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `created_at`, `updated_at`, `qty`, `sum`, `status`, `name`, `email`, `phone`, `address`, `text`) VALUES
(3, '2018-09-11 23:21:05', '2018-09-11 23:21:05', 1, 56, '0', 'Уы', 'shhh@mail.ru', 'рр', 'рр', '0'),
(4, '2018-09-12 00:00:42', '2018-09-12 00:00:42', 1, 56, '0', 'Mey', 'shhh@mail.ru', '6788', 'dghyhh', '0'),
(5, '2018-09-12 00:26:53', '2018-09-12 00:26:53', 1, 56, '0', 'li', 'shhh@mail.ru', '1123', 'dtuuui', '0'),
(6, '2018-09-12 00:29:04', '2018-09-12 00:29:04', 1, 56, '0', 'li', 'shhh@mail.ru', '1123', 'dtuuui', '0'),
(7, '2018-09-12 00:33:08', '2018-09-12 00:33:08', 2, 112, '0', 'Mei', 'medeyacom@mail.ru', 'fff', 'ggg', '0'),
(8, '2018-09-12 00:33:48', '2018-09-12 00:33:48', 2, 112, '0', 'Mei', 'medeyacom@mail.ru', 'fff', 'ggg', '0'),
(9, '2018-09-12 00:38:25', '2018-09-12 00:38:25', 1, 20, '0', 'lil', 'justfalk@mail.ru', '111', '111145', '0'),
(10, '2018-09-12 00:52:54', '2018-09-12 00:52:54', 1, 20, '0', 'lil', 'justfalk@mail.ru', '111', '111145', '0'),
(11, '2018-09-12 00:53:48', '2018-09-12 00:53:48', 1, 20, '0', 'lil', 'justfalk@mail.ru', '111', '111145', '0'),
(12, '2018-09-12 00:55:21', '2018-09-12 00:55:21', 1, 20, '0', 'lil', 'justfalk@mail.ru', '111', '111145', '0'),
(13, '2018-09-12 00:55:44', '2018-09-12 00:55:44', 1, 20, '0', 'lil', 'justfalk@mail.ru', '111', '111145', '0'),
(14, '2018-09-12 01:04:32', '2018-09-12 01:04:32', 1, 205, '0', 'lilian', 'justfalk@mail.ru', '789', 'tgii', '0'),
(15, '2018-09-12 01:07:41', '2018-09-12 01:07:41', 1, 100, '0', 'lil', 'justfalk@mail.ru', '7899', '79', '0'),
(16, '2018-09-12 01:20:44', '2018-09-12 01:20:44', 1, 100, '0', 'lil', 'justfalk@mail.ru', '6778', '776t', '0'),
(17, '2018-09-12 01:21:44', '2018-09-12 01:21:44', 1, 100, '0', 'lil', 'justfalk@mail.ru', '6778', '776t', '0'),
(18, '2018-10-05 16:33:31', '2018-10-05 16:33:31', 1, 56, '0', 'Уы', 'justfalk@mail.ru', '1123', 'dghyhh', '0'),
(19, '2018-10-07 14:38:49', '2018-10-07 14:38:49', 2, 112, '0', 'Тан', 'justfalk@mail.ru', '7890', 'dghyhh', 'Мне нужны штаны'),
(20, '2018-10-07 21:17:47', '2018-10-07 21:17:47', 2, 112, '0', 'Mey', 'justfalk@mail.ru', 'рр', 'dtuuui', 'Мне нужны штаны!'),
(21, '2018-10-07 21:51:09', '2018-10-07 21:51:09', 1, 56, '0', 'li', 'justfalk@mail.ru', '8966', '111145', 'Мне нужны штаны!!'),
(22, '2018-10-07 21:53:37', '2018-10-07 21:53:37', 1, 20, '0', 'Mei', 'justfalk@mail.ru', '7890', 'dghyhh', 'Мне нужны штаны!!!'),
(23, '2018-10-07 22:01:52', '2018-10-07 22:01:52', 1, 0, '0', 'li', 'justfalk@mail.ru', '7890', 'ff', 'Мне нужны штаны!!!'),
(24, '2018-10-08 00:43:56', '2018-10-08 00:43:56', 1, 56, '0', 'li', 'justfalk@mail.ru', '8966', 'dghyhh', 'Мне нужны штаны!!!');

-- --------------------------------------------------------

--
-- Структура таблицы `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `qty_item` int(11) NOT NULL,
  `sum_item` float NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `name`, `price`, `qty_item`, `sum_item`, `text`) VALUES
(1, 1, 6, 'Кардиган Levi\'s Icy Grey Heather M', 100, 2, 200, ''),
(2, 2, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 1, 56, ''),
(3, 3, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 1, 56, ''),
(4, 4, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 1, 56, ''),
(5, 5, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 1, 56, ''),
(6, 6, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 1, 56, ''),
(7, 7, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 2, 112, ''),
(8, 8, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 2, 112, ''),
(9, 9, 3, 'Блуза Mango 53005681-05 M Бежевая', 20, 1, 20, ''),
(10, 10, 3, 'Блуза Mango 53005681-05 M Бежевая', 20, 1, 20, ''),
(11, 11, 3, 'Блуза Mango 53005681-05 M Бежевая', 20, 1, 20, ''),
(12, 12, 3, 'Блуза Mango 53005681-05 M Бежевая', 20, 1, 20, ''),
(13, 13, 3, 'Блуза Mango 53005681-05 M Бежевая', 20, 1, 20, ''),
(14, 14, 12, 'Сумка Michael Kors Selma Золотистая', 205, 1, 205, ''),
(15, 15, 6, 'Кардиган Levi\'s Icy Grey Heather M', 100, 1, 100, ''),
(16, 16, 6, 'Кардиган Levi\'s Icy Grey Heather M', 100, 1, 100, ''),
(17, 17, 6, 'Кардиган Levi\'s Icy Grey Heather M', 100, 1, 100, ''),
(18, 18, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 1, 56, ''),
(19, 19, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 2, 112, ''),
(20, 20, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 2, 112, ''),
(21, 21, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 1, 56, ''),
(22, 22, 3, 'Блуза Mango 53005681-05 M Бежевая', 20, 1, 20, ''),
(23, 23, 5, 'Блузка Kira Plastinina 17-16-17453-SM-29 S', 0, 1, 0, ''),
(24, 24, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', 56, 1, 56, '');

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text,
  `price` float NOT NULL DEFAULT '0',
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT 'no-image.png',
  `hit` enum('0','1') NOT NULL DEFAULT '0',
  `new` enum('0','1') NOT NULL DEFAULT '0',
  `sale` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `category_id`, `name`, `content`, `price`, `keywords`, `description`, `img`, `hit`, `new`, `sale`) VALUES
(1, 4, 'Джинсы Garcia 244/32/856 28-32 р Серо-синие', '<p>Великолепные джинсы винтажно-голубого цвета. Настоящая находка для любителей качественного денима. Особенности: Зауженные к низу Пять карманов Высококачественный деним Высокая посадка (high fit) Выгодно подчеркивают фигуру<img alt=\"\" src=\"/web/upload/global/Lighthouse.jpg\" style=\"float:right; height:150px; margin-left:10px; margin-right:10px; width:200px\" /></p>\r\n', 10, '', '', 'product1.jpg', '0', '0', '0'),
(2, 4, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', '<p><strong>MR520 </strong>&ndash; амбициозный восточноевропейский бренд, который предлагает качественную и стильную одежду, сделанную специально для молодых людей, следящих за своим внешним видом. Женские джинсы фасона boyfriend fit (в переводе с англ. &ndash; &quot;джинсы моего парня&quot;). Модель с зауженными штанинами. Застегивается на пуговицы. Изделие с низкой посадкой. Джинсы дополнены прорезными карманами спереди и накладными карманами сзади. Изделие декорировано эффектом потертости, вареным эффектом и необычными швами.</p>\r\n', 56, '', '', 'product2.jpg', '1', '0', '0'),
(3, 9, 'Блуза Mango 53005681-05 M Бежевая', '<p><strong>Испанский бренд модной одежды &quot;Mango&quot;</strong><em> </em>родился в 1984 году в Барселоне, где и по сей день находится штаб-квартира компании. В том же городе появился и первый магазин &mdash; на улице Пасео де Грасия (Paseo de Gracia). Компания, основанная братьями Исааком и Нахманом Андиком (Isaac &amp; Nahman Andic), очень быстро набрала популярность &mdash; всего лишь годом позднее был открыт магазин в другом городе, на этот раз в Валенсии. Одежда &quot;Mango&quot; отличается высоким качеством, приемлемой ценой, современным дизайном и неповторимым стилем.</p>\r\n', 20, '', '', 'product3.jpg', '1', '1', '0'),
(4, 21, 'Блуза Tom Tailor TT 20312490071 7610 M Зелёная', '\r\n\r\nTom Tailor Group — один из ведущих и быстро развивающихся Fashion холдингов германии и европы, продукция которого ориентирована на целевую аудиторию в возрасте от 0 до 60 лет.\r\n\r\nTom Tailor известен на рынке текстиля с 1962 года и до сих пор сохраняет стандарты немецкого качества. Tom Tailor предлагает повседневную одежду и аксессуары высокого качества для женщин, мужчин и детей.\r\n\r\nОдежда от Tom Tailor поможет создать активный повседневный образ с нотками элегантности.', 70, NULL, NULL, 'product4.jpg', '1', '0', '1'),
(5, 25, 'Блузка Kira Plastinina 17-16-17453-SM-29 S', '', 0, '', 'блузка', 'product5.jpg', '1', '0', '0'),
(6, 28, 'Кардиган Levi\'s Icy Grey Heather M', '', 100, '', '', 'product6.jpg', '1', '0', '0'),
(7, 28, 'Кардиган ONLY ON 15102048 M Black Tan/Partridg', '<p>Casual марка молодежной женской одежды ONLY является частью датской компании Bestseller AS. Изначально Bestseller занимался производством детской одежды, а в 1995 году появилась на свет марка ONLY. Популярность этой марки возрастала быстрыми темпами и теперь ONLY владеет 770 магазинами в более чем 40 странах мира. ONLY &mdash; бренд стильной молодежной одежды. Это бренд для тех, кто любит добиваться успеха и быть не таким, как все. Демократичные цены, модные модели, экологически чистые ткани делают одежду от ONLY сверхпопулярной.</p>\r\n', 0, '', '', 'no-image.png', '1', '1', '0'),
(8, 26, 'Брюки SK House 2211-1972кор L Коричневые', '\r\n\r\nКомпания SK House — это украинский производитель модной женской одежды с безупречной репутацией и тысячами поклонников по всему СНГ. SK House изготавливает качественный и долговечный товар, созданный из высококачественных тканей. Компания использует современные методы пошива и, изучая текущие мировые тенденции и локальные требования покупателей, создает модели, которые не задерживаются на полках длительное время и быстро раскупаются во всех странах.', 99, NULL, NULL, 'no-image.png', '0', '0', '1'),
(9, 26, 'Брюки Kira Plastinina 17-07-17418-FL-58 L', NULL, 0, NULL, NULL, 'product1.jpg', '0', '0', '0'),
(10, 29, 'Сумка GUSSACI TUGUS13A060-3-10', 'Простота, инновационный стиль бренда, высококачественные требования к продукции, благодаря этому GUSSACI Italy пользуется высокой репутацией во многих странах Европы. Превосходное качество, отличный дизайн, соответствующие цены делают \"Гуссачи\" модным и популярным!\r\n\r\nОсобенности:\r\n\r\nКоличество основных отделений: 1. Сумка имеет прорезной карман на молнии, а также два небольших накладных кармана для хранения мобильного телефона, разных портативных гаджетов и мелочей. На лицевой стороне модели есть узкий прорезной карман на \"молнии\". На тыльной стороне модели есть прорезной карман на \"молнии\". Особенностью данной модели является возможность изменения ее формы при помощи дополнительной внешней застежки-молнии. Сумка имеет 2 ручки для переноса на локте или в руке. Их длина не регулируется и составляет 45 см, а высота от крайней точки ручки до сумки — 16 см. В комплект к изделию прилагается съемный плечевой ремень. Его длина может регулироваться при помощи металлической пряжки от 78 до 137.5 см. Сумка закрывается при помощи застежки-молнии.\r\n\r\nМатериал подкладки: плотная ткань.\r\nМатериал сумки: кожезаменитель.\r\nЦвет фурнитуры: золото.\r\nРазмеры сумки: 26 х 25 х 10.5 см', 15, NULL, NULL, 'product3.jpg', '0', '1', '0'),
(11, 29, 'Сумка Michael Kors Jet Set Travel Нежно-розовая', '\r\n\r\nОсобенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом.\r\n\r\nМодели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.', 200, NULL, NULL, 'no-image.png', '0', '0', '1'),
(12, 29, 'Сумка Michael Kors Selma Золотистая', '\r\n\r\nОсобенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом.\r\n\r\nМодели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.', 205, NULL, NULL, 'product5.jpg', '0', '0', '0'),
(13, 29, 'Сумка Michael Kors Bedford Красная', '\r\n\r\nОсобенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом.\r\n\r\nМодели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.', 0, NULL, NULL, 'no-image.png', '0', '0', '0'),
(14, 29, 'Сумка Michael Kors JS Travel Светло-розовая', '\r\n\r\nОсобенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом.\r\n\r\nМодели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.', 0, NULL, NULL, 'no-image.png', '0', '0', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `auth_key` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `auth_key`) VALUES
(1, 'Admin', '$2y$13$x0hJ12w5D6weXNzFygpiSeYq2AC9LSbsPI2pqjvNbLHsRBcU6Hmmi', '-ouLCZrxdIkaeU20V04MGI-JHZWWZbQx');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contactform`
--
ALTER TABLE `contactform`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `contactform`
--
ALTER TABLE `contactform`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
