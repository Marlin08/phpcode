<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 07.05.2016
 * Time: 10:35
 */
//виджет для работы с меню category
namespace app\components;
use yii\base\Widget;
use app\models\Category;
use Yii;

class MenuWidget extends Widget{

    public $tpl;
    public $model;
    public $data; // хранит все записи arr категорий из бд 
    public $tree; //рез. работы функции из массива arr, в ней видно, какая кат.вложена в какую
    public $menuHtml; //html шабло в зависимости от того, кот. сохр.в св-ве tpl - full или select

    public function init(){
        parent::init();
        /*что пришло в парам. если пользователь ничего не передал, то присваиваем по умолчанию menu */
        if( $this->tpl === null ){
            $this->tpl = 'menu';
        } // расширение шаблону menu
        $this->tpl .= '.php';
    }
// метод для вывода 
    // c пом. asArray получаем массивы из объектов
    //метод indexBy в поле id кот. должно совпадать c ключами
    public function run(){
         // get cache
        if($this->tpl == 'menu.php'){
        // обращаемся к классу cashe и его методу get
        // данные получаем по ключу кот.называем menu
        $menu = Yii::$app->cache->get('menu');
        // если что-то получено из кеша возвр.меню
        if($menu) return $menu;
        }

        $this->data = Category::find()->indexBy('id')->asArray()->all();
        // св-во tree в кот. хранится дерево
       $this->tree = $this->getTree();
        // debug($this->tree);
         // в св-во возвр. рез.работы getMenuHtml кот.перед. tree
        $this->menuHtml = $this->getMenuHtml($this->tree);
         //возвращаем то, что попало в tpl
        //return $this->tpl;
          // set cache
        // если кеша нет, то формируем меню
        // обр.к объекту арр классу и записываем кеш
        // в парам. перед. ключ-файл кеша и данные куда хотим записать и время
        if($this->tpl == 'menu.php'){
            Yii::$app->cache->set('menu', $this->menuHtml, 60);
        }
        return $this->menuHtml;
    }
// проходится циклом и из одномерного массива строит дерево
    // если необходимо Adidas вложить в Nike то нужно parent id c 1 поменять на 4 
     protected function getTree(){
        $tree = [];
        foreach ($this->data as $id=>&$node) {
            if (!$node['parent_id'])
                $tree[$id] = &$node;
            else
                $this->data[$node['parent_id']]['childs'][$node['id']] = &$node;
        }
        return $tree;
    }
        
// выводит опр. tpl 
 protected function getMenuHtml($tree, $tab = ''){
       // в пустую пере. пом.гот.html код
        $str = '';
        // проходимя в цикле по дереву или его куску 
        foreach ($tree as $category) {
            // вызываем метод и передаём ему каждый конкр.эл.
            $str .= $this->catToTemplate($category, $tab);
        }
        return $str;
    }

// метод прин. пара. переданный эл. и пом. его в шаблон
    protected function catToTemplate($category, $tab){
        // чтобы не произв.вывод шаблона в брауз. произв.буф.
        ob_start();
        include __DIR__ . '/menu_tpl/' . $this->tpl;
        // возвр. в перем str
        return ob_get_clean();
    }

} 
   
