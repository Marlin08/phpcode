/*price range*/

 $('#sl2').slider();

    $('.catalog').dcAccordion({
        speed: 300
    });

    function showCart(cart){
    	//обращаемся к модальному окну, внутри него к эл. с классом
    	//модал-боди и методом html в него вставляем ответ
        $('#cart .modal-body').html(cart);
        //чтобы показать окно исп.метод modal в idcart
        $('#cart').modal();
    }

// делегируем события для кнопки удалить, обращаясь к род.классу modal-body

	function getCart(){
        $.ajax({
            url: '/cart/show',
            type: 'GET',
            success: function(res){
                if(!res) alert('Ошибка!');
                showCart(res);
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    }

 $('#cart .modal-body').on('click', '.del-item', function(){
 	//получаем id товара кот.хотим удалить
        var id = $(this).data('id');
        //передаем данные на сервер 
        $.ajax({
        	//запрос  идёт в контр.cart и действ del-item
            url: '/cart/del-item',
            data: {id: id},
            type: 'GET',
            success: function(res){
                if(!res) alert('Ошибка!');
                showCart(res);
            },
            error: function(){
                alert('Error!');
            }
        });
    });


    function clearCart(){
        $.ajax({
            url: '/cart/clear',
            type: 'GET',
            success: function(res){
                if(!res) alert('Ошибка!');
                showCart(res);
            },
            error: function(){
                alert('Error!');
            }
        });
    }
	// передаём данные на сервер
    $('.add-to-cart').on('click', function (e) {
        //отменяем переход по ссылке
        e.preventDefault();
        //атрибут товара получаем из data id п.125 category/index
        var id = $(this).data('id');
        // пере.qty = знач.поля со знач.qty
         qty = $('#qty').val();
        $.ajax({
            url: '/cart/add',
            data: {id: id, qty: qty},
            // запрос идёт в CartControler п.38
            type: 'GET',
            success: function(res){
                if(!res) alert('Ошибка!');
                //console.log(res);
                // в функцию передаём ответ готовый вид
                showCart(res);
            },
            error: function(){
                alert('Error!');
            }
        });
    });

	var RGBChange = function() {
	  $('#RGB').css('background', 'rgb('+r.getValue()+','+g.getValue()+','+b.getValue()+')')
	};	
		
/*scroll to top*/

$(document).ready(function(){
	$(function () {
		$.scrollUp({
	        scrollName: 'scrollUp', // Element ID
	        scrollDistance: 300, // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top', // 'top' or 'bottom'
	        scrollSpeed: 300, // Speed back to top (ms)
	        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
	        animation: 'fade', // Fade, slide, none
	        animationSpeed: 200, // Animation in speed (ms)
	        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
					//scrollTarget: false, // Set a custom target element for scrolling to the top
	        scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
	        scrollTitle: false, // Set a custom <a> title if required.
	        scrollImg: false, // Set true to use image
	        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        zIndex: 2147483647 // Z-Index for the overlay
		});
	});
});
