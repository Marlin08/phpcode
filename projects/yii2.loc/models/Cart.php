<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 14.05.2016
 * Time: 10:40
 */
/*корзина*/
namespace app\models;
use yii\db\ActiveRecord;

class Cart extends ActiveRecord{
//поведение для вывода карт. 
	 public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public function addToCart($product, $qty = 1){
    	/*получаем гл.карт. для вывода */
    	 $mainImg = $product->getImage();
    	//если в arr cart есть эл.product
   		   if(isset($_SESSION['cart'][$product->id])){
   		   	// то добавляем то кол-во кот.пришло из пер.qty
            $_SESSION['cart'][$product->id]['qty'] += $qty;
            //если товара нет, то создаём arr
        }else{
            $_SESSION['cart'][$product->id] = [
                'qty' => $qty,
                'name' => $product->name,
                'price' => $product->price,
                'img' => $mainImg -> getUrl('x50')
            ];
        }
        //кол-во и итоговая сумма
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty'] + $qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum'] + $qty * $product->price : $qty * $product->price;
    }
		// метод принимает id товара
     public function recalc($id){
     	// проверяем: сущ.ли в сесии эл.в arr cart
        if(!isset($_SESSION['cart'][$id])) return false;
        // если сущ.то мы должны уменьшить итоговое кол-во из корзины, кот.уд.
        //в перем. ложим текущий session эл. и его кол-во - qty - то кол-во кот. будет уд.из итогового
        $qtyMinus = $_SESSION['cart'][$id]['qty'];
        // тоже самое для суммы - текущее кол-во умн.на цену товара
        //полученная сумма отнимается из итоговой п. 30 cart-sum
        $sumMinus = $_SESSION['cart'][$id]['qty'] * $_SESSION['cart'][$id]['price'];
        // выполнение вычитания
        $_SESSION['cart.qty'] -= $qtyMinus;
        $_SESSION['cart.sum'] -= $sumMinus;
        // удаление текущего товара
        unset($_SESSION['cart'][$id]);
    }


} 