<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 07.05.2016
 * Time: 10:28
 */

namespace app\models;
use yii\db\ActiveRecord;

error_reporting(E_ALL);

class Category extends ActiveRecord{

	public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public static function tableName(){
        return 'category';
    }
/*т.к. в одной категории может сод.много товаров то
это связь одного к hasMany*/
/* 2 парам. указ. по какому полю осущ.данная связь*/
/* => на какое поле ссылается данное поле в табл.*/
    public function getProducts(){
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

} 