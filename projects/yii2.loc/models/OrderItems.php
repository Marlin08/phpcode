<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "order_items".
 *
 * @property string $id
 * @property string $order_id
 * @property string $product_id
 * @property string $name
 * @property double $price
 * @property integer $qty_item
 * @property double $sum_item
 * @property string $text
 */
class OrderItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_items';
    }
// один товар может принадлежать только одному заказу
    public function getOrder(){
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'name', 'price', 'qty_item', 'sum_item',], 'required'],
            [['order_id', 'product_id', 'qty_item', 'text'], 'integer'],
            //[['comment']],
            [['price', 'sum_item'], 'number'],
            [['name','text'], 'string', 'max' => 255],
        ];
    }
}
