<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;


error_reporting(E_ALL);
 /**
 * This is the model class for table "contactform".
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property integer $subject
 * @property double $body
 * @property string $verifyCode
 
 */

class Contactform extends ActiveRecord
{
    public $id;
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;


     public static function tableName()
    {
        return 'contactform';
    }
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
       
           /* Поля обязательные для заполнения */
            [ ['name', 'email', 'subject', 'body'], 'required'],
            /* Поле электронной почты */
            ['email', 'email'],
            /* Капча */
            ['verifyCode', 'captcha', 'captchaAction'=>'index/captcha'],
          /*  ['verifyCode', 'captcha','captchaAction'=>'/contactus/default/captcha'],*/

        ];

    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Подтвердите код',
            'name' => 'Имя',
            'email' => 'Электронный адрес',
            'subject' => 'Тема',
            'body' => 'Сообщение',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
  

public function saveForm()
{
    if($this->save() && $this->validate()) {
         return $this->contact();
    }

 
   return false;
}


 /*public function saveForm($runValidation=true,$attributes=null)
    {
        if(!static::save($runValidation,$attributes))
        {
            throw new CDbException($this->errorSummary());
        }

 
    return true;
    }
 /*
try
{
    $model->saveForm();
    $this->redirect(array('index'));
}
catch(Exception $e)
{
    Yii::app->user->setFlash('error','DB Error');
}
$this->render('create',array('model'=>$model))

*/



 public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom(['medeyacom@mail.ru' => $this->name])
                ->setSubject('Сообщение от посетителя '.$this->name)
                ->setTextBody($this->body)
                ->send();

            return true;
        } else {
            return false;
        }
    }



                   
 }               





