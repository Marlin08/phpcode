<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 08.05.2016
 * Time: 10:00
 */

namespace app\controllers;
use yii\web\Controller;

class AppController extends Controller{
//метод для параметров title keywords description
	// вызывается в кач.парам. в том или ином действии
	// null - т.к. все парам. необязат.
    protected function setMeta($title = null, $keywords = null, $description = null){
    	// устанав. парам. обр. к this view и св-во title и назначаем то что передано в парам. title
	    $this->view->title = $title;
	    // регмстрируем метатег с именем keywords' и содерж. 'content то что передано в парам. key
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        // тоже самое для desc
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
    }

} 