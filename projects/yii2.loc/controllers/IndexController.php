<?php


namespace app\controllers;

use Yii;
use yii\App\Controller;
use app\models\ContactForm;
use yii\web\Request;




class IndexController extends AppController
{


  public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ]
        ];
    }
    public function actionContact()
        {
        //$this->layout = 'contact';
        $model = new ContactForm();
      /*  if ($model->load(Yii::$app->request->post()) && $model->contact(setting::ADMIN_EMAIL_ADDRESS))*/
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail']))
         {
            Yii::$app->session->setFlash('contactFormSubmitted');

 /*if ($model->load(Yii::$app->request->post()) && $model->saveForm()) {
            Yii::$app->session->setFlash('contactFormSubmitted');*/
           
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }   
  }
   

    

    


  
