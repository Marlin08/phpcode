<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 14.05.2016
 * Time: 10:37
 */

namespace app\controllers;
use app\models\Product;
use app\models\Cart;
use app\models\Order;
use app\models\OrderItems;
use Yii;

/*Array
(
    [1] => Array
    (
        [qty] => QTY
        [name] => NAME
        [price] => PRICE
        [img] => IMG
    )
    [10] => Array
    (
        [qty] => QTY
        [name] => NAME
        [price] => PRICE
        [img] => IMG
    )
)
    [qty] => QTY,
    [sum] => SUM
);*/

class CartController extends AppController{
// экшен корзины
    public function actionAdd(){
        //получаем id
        $id = Yii::$app->request->get('id');
        // получаем qty - данные от пользователя
        $qty = (int)Yii::$app->request->get('qty');
        // проверяем: если пер.qty возвр false т.е. 0 т.к. int то присваиваем знач. 1 в противном случае (:) положим то что ввёл польз.
        $qty = !$qty ? 1 : $qty;
        $product = Product::findOne($id);
        if(empty($product)) return false;
        $session =Yii::$app->session;
        $session->open();
        //создаем объект getcart в модели
        $cart = new Cart();
        // в модели Cart вызываем метод и принимаем qty - ложим товар в корзину
        $cart->addToCart($product, $qty);
        //если запрос не ajax
         if( !Yii::$app->request->isAjax ){
            //редирект на ту стр.с кот.пришёл польз. в Yii хранится адр.url с кот.пришёл польз.
            return $this->redirect(Yii::$app->request->referrer);
        }
        $this->layout = false;
        // генерируем вид в кот.записывается корзина
        return $this->render('cart-modal', compact('session'));
        
    }

      public function actionClear(){
        $session =Yii::$app->session;
        $session->open();
        $session->remove('cart');
        $session->remove('cart.qty');
        $session->remove('cart.sum');
        $this->layout = false;
        return $this->render('cart-modal', compact('session'));
    }
    // действие del-item после - буква пишется в верхнем рег.
    // экшен для Ajax запроса из п.27 main.js
      public function actionDelItem(){
        // получаем id товара и открываем сессию
        $id = Yii::$app->request->get('id');
        $session =Yii::$app->session;
        $session->open();
        // метод описывается в модели карт
        $cart = new Cart();
        // метод для пересчёта пом.id получ.
        $cart->recalc($id);
        $this->layout = false;
        // после рекальк.возвр.вид корзины
        return $this->render('cart-modal', compact('session'));
    }
        // для значка корзины сверху
       public function actionShow(){
        $session =Yii::$app->session;
        $session->open();
        $this->layout = false;
        return $this->render('cart-modal', compact('session'));
    }
// вид для покупок в корзине
    public function actionView(){
       // debug(Yii::$app->params['adminEmail']);
        $session =Yii::$app->session;
        $session->open();
        // заголовок корзины
        $this->setMeta('Корзина');
        // создаём модель заказа для формы кроме sum qty
        //status- автомат.присваевается. created updated - автомат.зап.
        $order = new Order();
        // загружаем данные из формы
        if( $order->load(Yii::$app->request->post()) ){
            //debug(Yii::$app->request->post());
            // заполняем недостающие данные
            // qty sum = тому что в сессии
            // id хранится в объекте order и его св-ве id
            $order->qty = $session['cart.qty'];
            $order->sum = $session['cart.sum'];
            // если заказ сохранён
            if($order->save()){
                //вызываем метод и передаем корзину и св-во id
                $this->saveOrderItems($session['cart'], $order->id);
                Yii::$app->session->setFlash('success', 'Ваш заказ принят. Менеджер вскоре свяжется с Вами.');
                //в парам.ук. вид order и передаём корзину
                Yii::$app->mailer->compose('order', ['session' => $session])
                // с какого email получается почта, то что ук. в config/web
                    ->setFrom(['medeyacom@mail.ru' => 'yii2.loc'])
                    // куда 
                    ->setTo($order->email)
                    ->setSubject('Заказ')
                    //->setTextBody('Ваш заказ поступил в обработку!')
                    // выводит только текст
                   // ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
                    ->send();
                    // для отправки admin необходимо продублировать в setTo email admin config/params
                //очищаем корзину
                $session->remove('cart');
                $session->remove('cart.qty');
                $session->remove('cart.sum');
                // перезагрузка стр.
                return $this->refresh();
            }else{
                // флешки в cart/view
                Yii::$app->session->setFlash('error', 'Ошибка оформления заказа');
            }
        }

        return $this->render('view', compact('session', 'order'));
    }
    // метод принимает корзину и id сохр.заказа
      protected function saveOrderItems($items, $order_id){
        //проходим по массиву корзины карт и получаем id товара и инфо о заказе
        foreach($items as $id => $item){
            //создаем объект модели
            $order_items = new OrderItems();
            //id заказа
            $order_items->order_id = $order_id;
            // id товара
            $order_items->product_id = $id;
            $order_items->name = $item['name'];
            $order_items->price = $item['price'];
            $order_items->qty_item = $item['qty'];
            //кол-во умн.на цену
            $order_items->sum_item = $item['qty'] * $item['price'];
            $order_items->save();
        }
    }

} 