<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 09.05.2016
 * Time: 10:50
 */
// вывод карточки товара
namespace app\controllers;
use app\models\Category;
use app\models\Product;
use Yii;

class ProductController extends AppController{
//получаем id продукта из arr get
    public function actionView($id){
        $id = Yii::$app->request->get('id');
        // получаем из бд dc. всю инфу по товару и инфу о кат.
        // 1 вар. ленивая загрузка 
        $product = Product::findOne($id);
         if(empty($product))
            throw new \yii\web\HttpException(404, 'Такого товара нет');
//        $product = Product::find()->with('category')->where(['id' => $id])->limit(1)->one();
        // 2 вар. жадная загрузка в with - та связь кот. указ. в модели
//        $product = Product::find()->with('category')->where(['id' => $id])->limit(1)->one();
        //получаем все из табл.продуктов где поле хит =1 передаём их в render
        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();
        //title в назв.стр.
        $this->setMeta('E-SHOPPER | ' . $product->name, $product->keywords, $product->description);
        // возвр.шаблон с парам. объекта product
        return $this->render('view', compact('product','hits'));
    }

} 