<?php


namespace app\controllers;
use app\models\Category;
use app\models\Product;
use Yii;
use yii\data\Pagination;

class CategoryController extends AppController{

    public function actionIndex(){
    	//где поле hit = 1 в '' т.к. тип поля в базе enum текст,
    	// если integer то без '' limit 6 как в макете
        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();
        // вызываем метод и уст.название 
        $this->setMeta('E-SHOPPER');
        //массив hits доступен в шаблоне теперь пройдемся в цикле
        return $this->render('index', compact('hits'));
    }
    /* id - принимает числовой парам из п.51 config/web   'category/<id:\d+>' => 'category/view',*/
    // просмотр категорий
    // 1 вар. получение парам.из роутов config/web п.52
     public function actionView($id){
     	// 2 вар.получения id из arr get т.е. из адресной стр.
        //$id = Yii::$app->request->get('id');
        // данные для категории
        $category = Category::findOne($id);
        // если пуст arr category
         if(empty($category))
            throw new \yii\web\HttpException(404, 'Такой категории нет');
        //debug($id);
        //получаем продукты по номеру id
        // обращаемся к модели prodact задаем усл. где поле кат.id в табл. продуктов = текущему переданному id
        //$products = Product::find()->where(['category_id' => $id])->all();
        //создаем объект запроса для пагинации
         $query = Product::find()->where(['category_id' => $id]);
         //создаем объект класса pagination к методу tc который посчитает кол-во записей. ук. парам.pageSize и нужное кол-во для вывода. 
         $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 3, 'forcePageParam' => false, 'pageSizeParam' => false]);
         // выполняем запрос с какой записи и сколько
         $products = $query->offset($pages->offset)->limit($pages->limit)->all();

       
        // выводим title E-SHOPPER и прицепляем название категории
         $this->setMeta('E-SHOPPER | ' . $category->name, $category->keywords, $category->description);

        // рендерим category/view и передаем в него найденные продукты
        return $this->render('view', compact('products', 'pages', 'category'));
    }

     public function actionSearch(){
     	// получаем запрос из адресной стр.
        $q = trim(Yii::$app->request->get('q'));
        //находим данные по этому запросу
        // формирование title
        $this->setMeta('E-SHOPPER | Поиск: ' . $q);
        // если в запросе ничего нет, то ничего не выведет
        if(!$q)
            return $this->render('search');
        // оператор like поле name = query
        $query = Product::find()->where(['like', 'name', $q]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 3, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        // вид search перем.prodacts pages q
        return $this->render('search', compact('products', 'pages', 'q'));
    }

} 