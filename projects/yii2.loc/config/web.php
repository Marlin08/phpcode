<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    // контроллер по умолчанию вместо site/index для польз.части
    'defaultRoute' => 'category/index',
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'layout' => 'admin',
            'defaultRoute' => 'order/index',
                 ],
        /*'contactus'=> [
            'class' => 'app\modules\contactus\Module',
            'layout' => 'contact',
            'defaultRoute' => 'default/index',  
        ],*/
        /* 'user' => [
            'class' => 'app\modules\contactus\Module',
        ],*/
         'yii2images' => [
            'class' => 'rico\yii2images\Module',
            //be sure, that permissions ok
            //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
            'imagesStorePath' => 'upload/store', //path to origin images
            'imagesCachePath' => 'upload/cache', //path to resized copies
            'graphicsLibrary' => 'GD', //but really its better to use 'Imagick'
            'placeHolderPath' => '@webroot/upload/store/no-image.png', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '6H8SRuk9HIPbdCxI7C0lt16sl9rA1luC',
            'baseUrl'=>'',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',//если свой класс user, то здесь переименовать
            'enableAutoLogin' => true, //авторизация польз.на основе куки
// куда будет перенапр.польз.если неавтор.      
   //'loginUrl' => 'cart/view'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
         'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false, //если false то письма будут отпр. если true то в папке runtime 
            'transport'=>[
                'class' => 'Swift_SmtpTransport',     
                'host' => 'smtp.mail.ru',
                'port' => '465', // для mail.ru
                'encryption' => 'ssl', // tls
                'username' => 'medeyacom@mail.ru',
                'password' => 'severin888',
                
          //     'host' => 'smtp.gmail.com',
              // 'options' => array('hostname' => 'smtp.gmail.com',*/
          /*     'username' => 'livadii17@gmail.com',
                'password' => 'Severin777',
                'port' => '25',
                "encryption" => ("tls"),*/
               //'viewPath' => '@common/mail',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            // более конкретные правила должны нах.впереди более общих
            // page и номер стр.
              'category/<id:\d+>/page/<page:\d+>' => 'category/view',
            //ссылка соотв. контр.cat и экшену view
                 'category/<id:\d+>' => 'category/view',
                 //для ссылки карточки товаров
                 'product/<id:\d+>' => 'product/view',
                 'search/<id:\d+>' => 'category/search',
           ],
        ],
        
    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'], //доступ к редактору только для авт.польз.
            'root' => [
                'baseUrl'=>'/web',//папка web upload добавляет сам.
//                'basePath'=>'@webroot',
                'path' => 'upload/global',//куда загружается файл
                'name' => 'Global'//название папки для загрузки в редакторе
                // на хост.дать права на запись
            ],
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
