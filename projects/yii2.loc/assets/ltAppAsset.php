<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
  /* if lt IE 9  для подкл. скрипт. п.27 main.php html15shiv.js   respond.min.js*/
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ltAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/html5shiv.js',
        'js/respond.min.js',
    ];
//если по усл. подкл. стили, то cssOptions
    public $jsOptions = [
        'condition' => 'lte IE9',
        //подкл.в хедере
        'position' => \yii\web\View::POS_HEAD
    ];
}
