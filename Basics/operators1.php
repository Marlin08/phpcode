<?php

error_reporting(-1);

/*
Арифметически операторы
"+" сложение $a + $b
"-" вычитание $a - $b
"*" умножение $a * $b
"/" деление $a / $b
"-$a" отрицание (смена знака $a)
"$a % $b" деление по модулю (остаток от деления)
"$a ** $b" возведение в степень
"=" присваивание (установка значения)
"&" присваивание по ссылке
============================
"++$a" префиксный инкремент
"$a++" постфиксный инкремент
"--$a" префиксный декремент
"$a++" постфиксный декремент
"." конкатенация (склеивание строк)
комбинированные операторы
*/

// echo -5 - 3 * 5;
// echo 5 / 3;
// echo 10 % 3;
// echo 2 ** 3;
// echo pow(2, 4);
/*
$a = 5;
$b = $a;
var_dump($a);
$a = 7;
var_dump($a);
var_dump($b);
*/
$a = 5;
$b = &$a;
var_dump($a);
$b = 7;
var_dump($a);
var_dump($b);

//echo 5 + 't3est';